<html>

<title>
    Factorial
</title>
</html>

<?php

class factorial_of_a_number
{
    protected $_number;

    public function __construct($number)

    {

        if (!is_int($number))

        {

            throw new InvalidArgumentException('Not a number or missing argument');

        }

        $this->_number = $number;
    }

    public function result(){
        $a=1;
        for($b=1;$b<=$this->_number;$b++)
        $a=$a*$b;
        return $a;

    }

}
$newfactorial = New factorial_of_a_number(5);

echo $newfactorial->result();